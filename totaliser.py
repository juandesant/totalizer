#!/usr/bin/env python
import sys
import argparse
from pprint import pprint

# Define main help descriptor
parser = argparse.ArgumentParser(description='Calculate totals from numbers in each line on a file.')

# Define rest of the arguments
# Defaults to none in a non-interactive shell, stdin if argument not provided
parser.add_argument(
    '-i', '--input-file',
    type=argparse.FileType('r'),
    default=(None if sys.stdin.isatty() else sys.stdin)
)

# Allows to consider numbers as floats; default is integers
parser.add_argument(
    '-f', '--as-floats', 
    default=False,
    action="store_true"
)

# Allows to pass a previous total, in case we want to keep accumulating
parser.add_argument(
    '-t', '--previous-total',
    type=float,
    default=0
)

# Parse arguments
args = parser.parse_args()

# Start processing lines
errors = 0
total = args.previous_total
while True:
    line = args.input_file.readline()
    if not line: break
    try:
        if not args.as_floats:
            number = int(line.rstrip())
        else:
            number = float(line.rstrip())
        total += number
    except ValueError:
        errors += 1
        continue

if not args.as_floats:
    total = int(total)

print(total)
if errors > 0:
    error_plural=""
    if errors > 1:
        error_plural="s"
    print(
        f"{errors} error{error_plural} found while processing {args.input_file}",
        file=sys.stderr
    )